package com.thanhpd56.app;

import com.thanphd56.ui.UI;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by PHANTHANH on 8/21/2015.
 */
public class Application {
    List<Game> gameList = new ArrayList<Game>();
    private UI ui;

    public void setUi(UI ui) {
        this.ui = ui;
    }

    public void run() {
        while (true) {
            this.next();
        }
    }

    public void next() {
        String command = null;
        command = ui.getUserInput();
        if (command.equals("start x")) {
            startNewGame("x", "o");
        } else if (command.equals("start o")) {
            startNewGame("o", "x");
        } else if (command.equals("end")) {
            stopRunningGame();
        }else if(command.equals("history")){
            printHistory();

        } else{
            Pattern pattern = Pattern.compile("(\\d)\\s+(\\d)");
            Matcher matcher = pattern.matcher(command);
            if (matcher.matches() && getNumberOfRunningGame()==1) {

                int row = Integer.parseInt(matcher.group(1));
                int col = Integer.parseInt(matcher.group(2));
                getCurrentRunningGame().move(row, col);
            }

        }


    }

    private void printHistory() {
        ui.printInfo(ui.readFile("data/history.dat"));
    }

    private void startNewGame(String firstSign, String secondSign) {
        if (getNumberOfRunningGame() == 0) {
            Game game = new Game();
            game.setUi(ui);
            game.begin(firstSign, secondSign);
            gameList.add(game);
        }else{
            ui.printError("One game is running! Ends it before create new game");
        }
    }

    private void stopRunningGame() {
        if (getNumberOfRunningGame() == 1) {
            gameList.get(gameList.size() - 1).endGame();

            // save the match into file

        }
    }


    public void setUI(UI ui) {
        this.ui = ui;
    }

    public int getNumberOfRunningGame() {
        if (gameList.isEmpty())
            return 0;
        if (gameList.get(gameList.size() - 1).getState() == GameState.RUNNING) {
            return 1;
        }
        return 0;
    }

    public Game getCurrentRunningGame() {
        if (getNewestGame().getState() == GameState.RUNNING)
            return getNewestGame();
        return null;
    }

    public Game getNewestGame() {
        return gameList.get(gameList.size() - 1);
    }

    public UI getUi() {
        return ui;
    }
}
