package com.thanhpd56.app;

/**
 * Created by PHANTHANH on 8/21/2015.
 */
public class Board {
    private static final int MAX_ROW = 3;
    private static final int MAX_COL = 3;
    private String[][] firstBoard = new String[MAX_ROW][MAX_COL];
    private String[][] secondBoard = new String[MAX_ROW][MAX_COL];


    public boolean isWinState() {
        return checkWinState(firstBoard) || checkWinState(secondBoard);
    }

    public boolean checkWinState(String[][] board) {
        // check diagonals
        if (isFilled(board, 1, 1) && isFilled(board, 2, 2) && isFilled(board, 3, 3)) {
            return true;
        }
        if (isFilled(board, 1, 3) && isFilled(board, 2, 2) && isFilled(board, 3, 1)) {
            return true;
        }

        // check columns and rows
        for (int i = 1; i <= MAX_COL; i++) {
            if (isFilled(board, i, 1) && isFilled(board, i, 2) && isFilled(board, i, 3)) {
                return true;
            } else if (isFilled(board, 1, i) && isFilled(board, 2, i) && isFilled(board, 3, i)) {
                return true;

            }

        }
        return false;
    }

    public void insertNewMoving(boolean isFirst, int row, int col, String sign) {
        if (isFirst)
            firstBoard[row - 1][col - 1] = sign;
        else
            secondBoard[row - 1][col - 1] = sign;
    }

    public int getNumberFilledCell() {

        int result = 0;
        for (int i = 1; i <= MAX_ROW; i++) {
            for (int j = 1; j <= MAX_COL; j++) {
                if (isFilled(firstBoard, i, j)) {
                    result++;
                }
                if (isFilled(secondBoard, i, j)) {
                    result++;
                }
            }
        }
        return result;
    }

    public boolean isFilled(String[][] board, int row, int col) {
        return board[row - 1][col - 1] != null;
    }

    public String visualization() {
        String s = "";
        for (int i = 0; i < MAX_ROW; i++) {
            for (int j = 0; j < MAX_COL; j++) {
                if (firstBoard[i][j] != null) {
                    s += firstBoard[i][j];
                } else if (secondBoard[i][j] != null) {
                    s += secondBoard[i][j];
                } else {
                    s += ".";
                }
                s += "\t";

            }
            s += "\n";
        }
        return s;
    }
}
