package com.thanhpd56.app;

import com.thanphd56.ui.UI;

import java.util.Arrays;

/**
 * Created by PHANTHANH on 8/21/2015.
 */
public class Game {

    int[] history = new int[9];
    int numberOfStep = 0;
    Board mBoard;
    private GameState currentGameState;
    private UI ui;
    private boolean isFirst = true;
    private String firstUserSign = "x";
    private String secondUserSign = "o";
    private String winner;

    public Game() {
        this.currentGameState = GameState.RUNNING;
        mBoard = new Board();
    }

    public GameState getState() {
        return currentGameState;
    }


    public void setUi(UI ui) {
        this.ui = ui;
    }

    public void checkState() {
        if (mBoard.isWinState()) {
            currentGameState = GameState.END_WIN;
            if (isFirst) {
                winner = firstUserSign;
            } else {
                winner = secondUserSign;
            }
            ui.printInfo(winner + " wins!");
        } else if (mBoard.getNumberFilledCell() == 9) {
            ui.printInfo("Draw!");
            currentGameState = GameState.END_DRAW;
        }
        ui.writeFile(this.getInfo());


    }

    public int getNumberFilledCell() {
        return mBoard.getNumberFilledCell();
    }


    public void begin(String firstUserSign, String secondUserSign) {
        ui.printInfo("Start new game - " + firstUserSign + " move first.");
        currentGameState = GameState.RUNNING;
        isFirst = true;
        this.firstUserSign = firstUserSign;
        this.secondUserSign = secondUserSign;
    }

    public void endGame() {
        ui.printInfo("End game");
        currentGameState = GameState.END;
        ui.writeFile(this.getInfo());

    }


    public void move(int row, int col) {

        String sign = isFirst ? firstUserSign : secondUserSign;
        mBoard.insertNewMoving(isFirst, row, col, sign);
        history[numberOfStep++] = 3 * (row - 1) + col;
        checkState();
        isFirst = !isFirst;
    }

    public String getFirstUserSign() {
        return firstUserSign;
    }

    public String getSecondUserSign() {
        return secondUserSign;
    }

    public String getWinner() {
        return winner;
    }

    public boolean isRunning() {
        if (this.currentGameState == GameState.RUNNING)
            return true;
        return false;
    }

    public void printBoard() {
        System.out.println(mBoard.visualization());
    }


    public String getInfo() {
        String s = "";
        s += "First Moving: " + firstUserSign + "\n";
        s += "Winner: " + (getState() == GameState.END_WIN ? winner : " draw!") + "\n";
        s += "Steps:" + Arrays.toString(history) + "\n";
        return s;
    }

    public int[] getHistory() {
        return history;
    }



}
