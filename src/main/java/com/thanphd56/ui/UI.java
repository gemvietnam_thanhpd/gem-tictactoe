package com.thanphd56.ui;

import org.apache.commons.io.FileUtils;

import java.io.*;

/**
 * Created by PHANTHANH on 8/21/2015.
 */
public class UI {

    private BufferedReader reader;

    public UI() {
    }

    public String getUserInput() {
        String command = null;
        InputStreamReader r = new InputStreamReader(System.in);
        reader = new BufferedReader(r);

        try {
            command = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return command;
    }

    public void setReader(BufferedReader mockedBufferReader) {
        this.reader = mockedBufferReader;
    }

    public void printInfo(String s) {
        System.out.println(s);
    }


    public void printError(String s) {
        System.out.println(s);
    }
    public String readFile(String fileName) {
        File file = new File(fileName);
        try {
            String fileContent = FileUtils.readFileToString(file);
            return fileContent;
        } catch (IOException e) {
            // if if is not exist then return empty string
            return "";
        }
    }

    public void writeFile(String info) {
        String fileName = "data/history.dat";
        FileWriter fw = null; //the true will append the new data
        try {
            fw = new FileWriter(fileName ,true);
            fw.write(info);//appends the string to the file
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
