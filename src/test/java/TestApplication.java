import com.thanhpd56.app.Application;
import com.thanhpd56.app.GameState;
import com.thanphd56.ui.UI;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;

import static org.junit.Assert.*;

/**
 * Created by PHANTHANH on 8/21/2015.
 */
public class TestApplication {
    public static final String HISTORY_FILE_NAME = "data/history.dat";
    BufferedReader mockedBufferReader;
    private Application application;
    private UI ui;

    @Before
    public void setup() {
        application = new Application();
        ui = PowerMockito.spy(new UI());
        application.setUI(ui);
        ui.setReader(mockedBufferReader);
        // delete history file
        File historyFile = new File(HISTORY_FILE_NAME);
        historyFile.delete();

    }

    public void mockConsoleInput(String input) {
        /*try {
            PowerMockito.when(mockedBufferReader.readLine()).thenReturn(input);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }

    private void nextCommand(String mockedInput) {
        mockConsoleInput(mockedInput);
        application.next();
    }

    @Test
    public void testActionGetNumberOfRunningGameWhenAddNewGameAndStopItThenReturn0() {
        nextCommand(TestGame.INPUT_START_X);
        nextCommand(TestGame.INPUT_END);
        assertEquals(0, application.getNumberOfRunningGame());
    }


    @Test
    public void testActionGetNumberOfRunningGameWhenAddMultipleGamesThenReturn1() {
        nextCommand(TestGame.INPUT_START_X);
        nextCommand(TestGame.INPUT_START_X);
        Mockito.verify(ui, Mockito.times(1)).printError("One game is running! Ends it before create new game");
        nextCommand(TestGame.INPUT_END);
        nextCommand(TestGame.INPUT_START_X);
        assertEquals(1, application.getNumberOfRunningGame());


    }

    @Test
    public void testWhenUsersMoveToWinState() {
        nextCommand(TestGame.INPUT_START_X);
        nextCommand("1 1");
        nextCommand("1 2");
        nextCommand("2 2");
        nextCommand("1 3");
        nextCommand("3 3");

        assertEquals(0, application.getNumberOfRunningGame());
        assertEquals(GameState.END_WIN, application.getNewestGame().getState());
    }

    @Test
    public void testUIReadHistoryFileWhenApplicationIsStartThenReturnEmptyString(){
        assertTrue(application.getUi().readFile(HISTORY_FILE_NAME).isEmpty());
    }

    @Test
    public void testShowHistoryWhenSomeGamesAreCreated(){
        String history = "First Moving: x\n" +
                "Winner: x\n" +
                "Steps:[1, 2, 5, 3, 9, 0, 0, 0, 0]\n" +
                "\n" +
                "First Moving: x\n" +
                "Winner:  draw!\n" +
                "Steps:[1, 2, 0, 0, 0, 0, 0, 0, 0]";
        Mockito.when(ui.readFile(HISTORY_FILE_NAME)).thenReturn(history);

        nextCommand(TestGame.INPUT_START_X);
        nextCommand("1 1");
        nextCommand("1 2");
        nextCommand("2 2");
        nextCommand("1 3");
        nextCommand("3 3");
        nextCommand(TestGame.INPUT_START_X);
        nextCommand("1 1");
        nextCommand("1 2");
        nextCommand(TestGame.INPUT_END);
        nextCommand("history");
        Mockito.verify(ui, Mockito.atLeastOnce()).printInfo(history);
    }


}
