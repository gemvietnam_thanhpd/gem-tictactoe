/**
 * Created by PHANTHANH on 8/21/2015.
 */

import com.thanhpd56.app.Game;
import com.thanhpd56.app.GameState;
import com.thanphd56.ui.UI;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;


import java.util.Arrays;

import static org.junit.Assert.*;

public class TestGame {

    public static final String INPUT_START_X = "start x";
    public static final String INPUT_END = "end";
    private Game game;
    private UI ui;

    @Before
    public void setup() {
        game = new Game();
        ui = PowerMockito.spy(new UI());
        game.setUi(ui);
    }



    @Test
    public void testWhenInitNewGameThenItsStateShouldBeRunning() {
        Assert.assertEquals(GameState.RUNNING, game.getState());
    }

    @Test
    public void testWhenUserFirstlyMoveTo1x1Cell() {
        assertEquals(0, game.getNumberFilledCell());
        game.move(1, 1);
        assertEquals(1, game.getNumberFilledCell());
    }

    @Test
    public void testWhenUsersEndGameAfterSomeSteps() {
        game.move(1, 1);
        game.move(1, 2);
        game.move(3, 3);
        game.endGame();
        assertEquals(GameState.END, game.getState());

    }

    @Test
    public void testWhenXUsersMoveToWinState() {
        game.begin("x", "o");
        game.move(1, 1);
        game.move(1, 2);
        game.move(2, 2);
        game.move(1, 3);
        game.move(3, 3);
        assertEquals(GameState.END_WIN, game.getState());
        assertEquals("x", game.getWinner());
    }

    @Test
    public void testWhenUsersMoveToTheEndOfGameWithoutWinner() {
        game.begin("x", "o");
        game.move(1, 1);
        game.move(2, 1);

        game.move(1, 2);
        game.move(1, 3);

        game.move(2, 2);
        game.move(3, 2);

        game.move(2, 3);
        game.move(3, 3);

        game.move(3, 1);
        assertEquals(GameState.END_DRAW, game.getState());
    }

    @Test
    public void testWhenOUserMoveFirst() {
        game.begin("o", "x");
        assertEquals("o", game.getFirstUserSign());
        assertEquals("x", game.getSecondUserSign());

    }

    @Test
    public void testHistoryWhenInitGame(){
        int[] steps = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        assertEquals(Arrays.toString(steps), Arrays.toString(game.getHistory()));
    }

    @Test
    public void testHistoryWhenUserMoveSomeSteps(){
        game.begin("x", "o");
        game.move(1, 1);
        game.move(1, 2);
        game.move(2, 2);
        game.move(1, 3);
        game.move(3, 3);
        int[] steps = {1, 2, 5, 3, 9, 0, 0, 0, 0};
       assertEquals(Arrays.toString(steps), Arrays.toString(game.getHistory()));

    }

    @Test
    public void testCallSaveGameInfoIntoFileWhenGameIsEnded(){
        game.begin("x", "o");
        game.move(1, 1);
        game.move(1, 2);
        game.move(2, 2);
        game.move(1, 3);
        game.move(3, 3);
        game.checkState();

        String history = "First Moving: x\n" +
                "Winner: x\n" +
                "Steps:[1, 2, 5, 3, 9, 0, 0, 0, 0]\n";
        Mockito.verify(ui, Mockito.times(1)).writeFile(history);
    }




}
